# pvc: Personal Virtual Computer

This is a somewhat weird project, aiming to *emulate* something like a computer architecture, with a processor, rom, ram, hard drive, and so on.

Everything will be written in Common Lisp, since that's what seems most fun :)

## Hardware specification

Since I'll be *emulating* real hardware, I will use much simpler devices, with much simple architectures, and try to make simple protocols. The idea is that every file is a piece of hardware, and depending on which files you open, you'll be *using* a different machine.

### Chosen hardware

|Component| Model    |
|---------|----------|
|CPU      | MOS 6502 |
